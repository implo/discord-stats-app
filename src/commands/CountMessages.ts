import { ApplicationCommandOptionType, Colors, CommandInteraction, EmbedBuilder, TextChannel, User } from "discord.js";
import { Discord, Slash, SlashGroup, SlashOption } from "discordx";
import { BehaviorSubject} from "rxjs";
import { injectable } from "tsyringe";
import MessageCache from "../cache/MessageCache";
import CacheController from "../cache/CacheController";
import { MessageGroupQuery, MessageQuery } from "../statistic/MessageQuery";

@Discord()
@SlashGroup( {name: "count", description: "counting functions"} )
@SlashGroup("count")
@injectable()
export default class CountMessages {

    constructor(
        private readonly cacheController: CacheController
    ) { }

    private async cacheChannelAndGetWithProgress(
        channel: TextChannel, interaction: CommandInteraction
    ): Promise<MessageCache> {
        const progress = new BehaviorSubject(0)
        progress.subscribe(async progress => {
            await interaction.editReply(`fetching: ${progress}`)
        })

        const messageCache = this.cacheController.messageCache
        await messageCache.addChannel(channel, progress)
        return messageCache
    }

    private get cache() {
        return this.cacheController.messageCache
    }

    @Slash({ name: 'by-channel', description: 'returns a pong' })
    async countMessages(
        @SlashOption({
            name: 'channel',
            description: 'stats will refer to this channel',
            type: ApplicationCommandOptionType.Channel,
            required: true
        })
        channel: TextChannel,
        interatction: CommandInteraction
    ): Promise<void> {
        await interatction.reply("fetching")
        const cache = await this.cacheChannelAndGetWithProgress(channel, interatction)
        const messages = new MessageQuery(cache.data)
                    .byChannel(channel.id)

        await interatction.editReply(`count: ${messages.count}`)
    }

    @Slash({name: 'by-user-and-channel', description: 'gives the count of messages sent by the user in the channel'})
    async countMessagesByUserAndChannel(
        @SlashOption({
            name: 'channel',
            description: 'counts messages in this channel',
            type: ApplicationCommandOptionType.Channel,
            required: true
        })
        channel: TextChannel,
        @SlashOption({
            name: 'user',
            description: 'counts messages of this user',
            type: ApplicationCommandOptionType.User,
            required: true
        })
        user: User,
        interaction: CommandInteraction
    ) {
        await interaction.reply("fetching")
        const cache = await this.cacheChannelAndGetWithProgress(channel, interaction)
        const messages = new MessageQuery(cache.data)
                    .byChannel(channel.id)
                    .byAuthor(user.id)

        await interaction.editReply(`count: ${messages.count}`)
    }

    @Slash({name: 'count-total', description: 'counts every message in the cache'})
    async countTotal(interaction: CommandInteraction) {
        await interaction.reply("fetching")
        const cache = this.cache
        await interaction.editReply(`count: ${cache.data.length}`)
    }


    @Slash({name: 'count-per-user', description: 'prints table of sent messages per user in a channel'})
    async countPerUserInChannel(
        @SlashOption({
            name: 'channel',
            description: 'counts messages in this channel',
            type: ApplicationCommandOptionType.Channel,
            required: true
        })
        channel: TextChannel,
       interaction: CommandInteraction
    ) {
        await interaction.reply("fetching")
        const cache = await this.cacheChannelAndGetWithProgress(channel, interaction)
        const result = new MessageQuery(cache.data)
                    .byChannel(channel.id)
                    .groupByAuthor()
                    .sum()
        const embed = this.buildGroupByTable(result)
        await interaction.editReply({embeds: [embed]})
    }

    private buildGroupByTable(data: Map<string, number>) {
        const embed = new EmbedBuilder()
                .setTitle("statistic")
                .setColor(Colors.Red)

        data.forEach((value, key) => {
            embed.addFields([{name: key, value: value.toString()}])
        })

        return embed
    }

}
