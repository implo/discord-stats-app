import { Channel, Message } from "discord.js";

export class MessageQuery {

    constructor(
        private readonly messages: Message[]
    ) {}

    byChannel(channelId: string): MessageQuery {
        return new MessageQuery(this.messages.filter(
            message => message.channel.id === channelId
        ))
    }

    byAuthor(authorId: string): MessageQuery {
        return new MessageQuery(this.messages.filter(
            message => message.author.id === authorId
        ))
    }
    
    get count() {
        return this.messages.length
    }

    groupByAuthor(): MessageGroupQuery {
        const map = new Map<string, Message[]>

        this.messages.forEach(message => {
            this.groupMessage(message.author.id, message, map)
        })

        return new MessageGroupQuery(map)
    }

    groupByChannel(): MessageGroupQuery {
        let map = new Map<string, Message[]>

        this.messages.forEach(message => {
            map = this.groupMessage(message.channel.id, message, map)
        })

        return new MessageGroupQuery(map)
    }

    private groupMessage(key: string, message: Message, map: Map<string, Message[]>) {
        const group = map.get(key)

        if (group) {
            group.push(message)
        } else {
            map.set(key, [message])
        }

        return map
    }

}

export class MessageGroupQuery {

    constructor(
        private readonly groups: Map<string, Message[]>
    ) { }

    sum(): Map<string, number> {
        let sumMap = new Map<string, number>()

        this.groups.forEach((group, key) => {
            sumMap.set(key, group.length)
        })

        return sumMap 
    }

}
