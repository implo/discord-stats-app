import { Message } from "discord.js";

export default class CountUserMessages {
    constructor(
        private readonly data: Message[],
        private readonly userId: string
    ) {}

    count() {
        const filtered = this.data.filter(message => message.author.id === this.userId)
        return filtered.length
    }
}
