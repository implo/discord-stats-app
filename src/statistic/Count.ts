import { Message } from "discord.js";

export default class CountStatistic {
    constructor(private readonly messages: Message[]) { }


    count() {
        return this.messages.length
    }

}
