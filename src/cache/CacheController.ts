import { Message } from "discord.js";
import { ArgsOf, Discord, On} from "discordx";
import {injectable} from "tsyringe";
import MessageCache from "./MessageCache";

@Discord()
@injectable()
export default class CacheController {
	public readonly messageCache: MessageCache = new MessageCache()


    @On({event: 'messageCreate'})
    onMessage([message]: ArgsOf<'messageCreate'>) {
        this.messageCache.onMessage(message)
    }
}
