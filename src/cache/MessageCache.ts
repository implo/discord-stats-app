import {Message, TextChannel} from "discord.js";
import {BehaviorSubject} from "rxjs";

export default class MessageCache {
	private messagesCache: Message[] = []
    private cachedChannels: string[] = []

    get data() {
        return this.messagesCache
    }

	async addChannel(channel: TextChannel, progress: BehaviorSubject<number>) {
        if (!this.channelIsCached(channel)) {
            this.cachedChannels.push(channel.id)
            await this.addChannelForce(channel, progress)
        } 
	}

    private async addChannelForce(channel: TextChannel, progress: BehaviorSubject<number>) {
		let batch = await channel.messages.fetch({limit: 1})
		let last = batch.last()

		do {
			if (last === undefined) {
				return
			}

			this.messagesCache.push(...batch.values())
			batch = await channel.messages.fetch({before: last.id})
			last = batch.last()
			progress.next(this.messagesCache.length)
		} while(batch.size !== 0) 
    }

    channelIsCached(channel: TextChannel) {
        return this.cachedChannels.includes(channel.id)
    }

    addMessage(message: Message) {
        if(this.messagesCache) {
            this.messagesCache.push(message)
        }
    }

    onMessage(message: Message) {
        if (this.cachedChannels.includes(message.channel.id)) {
            this.messagesCache.push(message)
        }
    }
}
